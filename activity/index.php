<?php require_once "./code.php"?>


<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>S02 : Selection Control Structure and Array Manipulation</title>

	</head>
	<body>

		<h1>Divisibles by 5</h1>
		<?php forLoop() ?>


		<h1>Array Manipulation</h1>

		<?php $students = []; ?>
		<?php array_push($students, 'John Smith');?>
		<p><?php print_r ($students) ?></p>
		<p><?php echo count($students) ?></p>

		<?php array_push($students, 'Jane Smith');?>
		<p><?php print_r ($students) ?></p>
		<p><?php echo count($students) ?></p>

		<?php array_shift($students); ?>
		<p><?php print_r ($students) ?></p>
		<p><?php echo count($students) ?></p>


	</body>
</html>