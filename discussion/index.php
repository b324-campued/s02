<?php require_once "./code.php"?>


<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>S02 : Selection Control Structure and Array Manipulation</title>

	</head>
	<body>

		<h1>Repetition Control Structures</h1>
			
		<h2>While Loop</h2>
		<?php whileLoop() ?>


		<h2>Do-While Loop</h2>
		<?php doWhileLoop() ?>

		<h2>For Loop</h2>
		<?php forLoop() ?>

		<h2>Continue and Break Statement</h2>
		<?php modifiedForLoop() ?>

		<h1>Array Manipulation</h1>

		<h2>Types of Arrays</h2>

		<h3>Simple Array</h3>

		<ul>
			<!-- Like EJS templating   -->
			<?php foreach($computerBrands as $brand) {?>
			<li><?= $brand ?></li>
			<?php } ?>
		</ul>


		<h3>Associative Array</h3>

		<ul>
			<?php foreach($gradePeriods as $period => $grade) {?>
			<li>Grade in <?= $period ?> is <?= $grade ?></li>
			<?php } ?>
		</ul>

		<h3>Multidimensional Array</h3>

		<ul>
			<?php 
				foreach($heroes as $team){
					foreach($team as $member){
					?>
					<!-- In PHP, you need to close the PHP code whenever you want to switch back to writing HTML or any other non-PHP content. In your example, you are correctly opening and closing the PHP tags as needed. -->
						<li><?php echo $member ?></li>
					<?php
					}
				}
			?>
		</ul>

		<h3>Multidimensional Associative Array</h3>

		<ul>
			<?php 
				foreach($ironManPowers as $label => $powerGroup){
					foreach($powerGroup as $power){
						?>
							<li><?= "$label: $power" ?></li>
						<?php
					}
				}
			?>
		</ul>

		<h1>Mutators</h1>
		<h3>Sorting</h3>
		<p><?php print_r ($sortedBrands) ?></p>
		<p><?php print_r ($reverseSortedBrands) ?></p>

		<h3>Push</h3>
		<?php array_push($computerBrands, 'Apple'); ?>
		<p><?php print_r ($computerBrands) ?></p>

		<h3>Unshift</h3>
		<!-- PHP still like JS top down approach or syncrhonous -->
		<?php array_unshift($computerBrands, 'Dell'); ?>
		<p><?php print_r ($computerBrands) ?></p>

		<h3>Pop</h3>
		<?php array_pop($computerBrands); ?>
		<p><?php print_r ($computerBrands) ?></p>

		<h3>Shift</h3>
		<?php array_shift($computerBrands); ?>
		<p><?php print_r ($computerBrands) ?></p>

		<h3>Count</h3>
		<p><?php echo count($computerBrands) ?></p>


		<h3>In array</h3>
		<p><?php var_dump(in_array('Acer', $computerBrands)) ?></p>



	</body>
</html>