<?php

// Repetition Control Structures
	// Used to execute code multiple times

	// While Loop
		// While loop takes a single condition

function whileLoop() {
	$count = 5;

	while($count !== 0) {
		echo $count. '<br/>';
		$count--;
	}
}


	// Do-While Loop
		// Works like while loop but do-while executes at least once.

function doWhileLoop() {
	$count = 20;

	do {
		echo $count.'<br/>';
		$count--;
	} while ($count > 0);
}


	// For Loop 
		// for (initialValue; condition; iteration) { //codeblock }


function forLoop() {
	for($count = 0; $count <=20; $count++) {
		echo $count.'<br/>';
	}
}


	// Continue and Break Statements
		// Continue is keyword that allows the code to go to next loop without finishing the current code block (continue loop before the next line of code)
		// Break stop the execution for the current loop

function modifiedForLoop() {
	for($count = 0; $count <= 20; $count++ ) {
		if ($count % 2 === 0) {
			// continue doing the loop/ continue to iterate and dont move to next code
			continue;
		}
		// If not do this and continue iteration
		echo $count.'<br/>';

		if($count > 10) {
			break;
		}
	}	
}	


// Array Manipulation
	// Arrays can be declared using array() function or square brackets '[]'.

	// In earlier version we cannot use []. But as of Php 5.4 we can use the short array syntax which replace array() with[].

$studentNumbers = array('2020-1923', '2020-1924', '2020-1965', '2020-1926', '2020-1927');
$grades = [98.5, 94.3, 89.2, 90.1];
$computerBrands = ['Acer', 'Lenovo', 'Toshiba', 'Asus'];

// Associative Array

	// differ from numeric array in the sense that associative arrays uses descriptive names in naming the elements/values (key=>value pair)
	// Double arrow operator(=>) an assignment operator that is commonly used in creation of assiciative array
$gradePeriods = ['firstGrading' => 98.5, 'secondGrading' => 94.3, 'thirdGrading' => 89.2, 'fourthGrading' => 90.1 ];


// Two Dimensional Array
	// used in image processing, good example of this is our vviewing screen that uses multidimensional array of fixels

$heroes = [
	['ironman', 'thor', 'hulk'],
	['naruto', 'sasuke', 'hinata'],
	['goku', 'vegeta', 'beerus']
];

// Two Dimensional Associative Array

$ironManPowers = [
	// 'regular' and 'signature' will be label
	// 'power' is the value in each label
	'regular' => ['repulsor blast', 'rocket punch'],
	'signature' => ['unibeam']
];


// Array Iterative Method: foreach();
// each and in every iteration items is attached to $hero
foreach ($heroes as $hero) {
	// Iterate to items in the array
	print_r($hero);
}

foreach ($heroes as $hero => $specificHero) {
	// Iterate also inside the items
	foreach($specificHero as $individualHero) {
		print_r("$individualHero ");
	}
}

// Array Mutators
// Array Mutators modify contents of an array

// Array Sorting
$sortedBrands = $computerBrands;
$reverseSortedBrands = $computerBrands;
sort($sortedBrands);
rsort($reverseSortedBrands);





?>

